import React from 'react';
//import { ProgressBar } from 'logic/progress';
import { ProgressBar } from 'visual/progress-bar';


export class Playground extends React.Component {
    constructor() {
        super();
    }

    render() {
                /*
                <ProgressBar
                    height='50px'
                    background='brown'
                    label=' '
                    fill='light-brown'
                    progress={25}
                />*/
        return (
            <div>
                <ProgressBar percent={0.5} fillColor='light-brown' emptyColor='brown'>
                    <div className='flex-container align-center full-height'>
                        <div className='flex-sm-12 text-center text-white'>
                            0.5
                        </div>
                    </div>
                </ProgressBar>
            </div>
        );
    }
}

/*
const mapStateToProps = (state) => ({
    playing: state.selected.playing
});
*/
