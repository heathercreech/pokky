import { SELECT_AUDIO, PLAY_AUDIO, PAUSE_AUDIO } from './types';


export const selectAudio = (id) => ({ type: SELECT_AUDIO, id });
export const playAudio = () => ({ type: PLAY_AUDIO });
export const pauseAudio = () => ({ type: PAUSE_AUDIO });
export const toggleAudio = () => ({ type: TOGGLE_AUDIO });
