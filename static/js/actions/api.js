import fetch from 'isomorphic-fetch';
import { REQUEST_EPISODES, RECEIVE_EPISODES, RECEIVE_PODCASTS } from './types';

export const requestEpisodes = (podcastId) => ({
    type: REQUEST_EPISODES,
    podcastId
});

export const receiveEpisodes = (podcastId, json) => ({
    type: RECEIVE_EPISODES,
    podcastId,
    episodes: json.episodes,
    meta: json.meta
});

export const requestEpisodesFail = () => ({});


export const receivePodcasts = (json) => ({
    type: RECEIVE_PODCASTS,
    podcasts: json.podcasts
});


export function fetchEpisodes(podcastId) {
    return function(dispatch) {
        dispatch(requestEpisodes(podcastId));
        return fetch(`/api/v1/podcast/${podcastId}/episodes`)
            .then(response => response.json())
            .then(json => dispatch(receiveEpisodes(podcastId, json)));
    };
}


export function fetchFromApi(url, receiveAction) {
    return function(dispatch) {
        return fetch(url)
            .then(response => response.json())
            .then(json => dispatch(receiveAction(json)));
    };
}
