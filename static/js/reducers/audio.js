import { SELECT_AUDIO, PLAY_AUDIO, PAUSE_AUDIO, TOGGLE_AUDIO } from '../actions/types';


export function audio(state={}, action) {
    switch (action.type) {
    case SELECT_AUDIO:
        return { ...state, id: action.id };
    case PLAY_AUDIO:
        return { ...state, playing: true };
    case PAUSE_AUDIO:
        return { ...state, playing: false };
    case TOGGLE_AUDIO:
        RETURN { ...state, playing: !state.playing };
    default:
        return state;
    }
}
