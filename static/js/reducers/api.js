import {
    REQUEST_EPISODES,
    RECEIVE_EPISODES,
    RECEIVE_PODCASTS
} from '../actions';


export function episodes(state={}, action) {
    switch (action.type) {
    case REQUEST_EPISODES:
        return { ...state, isFetching: true };
    case RECEIVE_EPISODES:
        return { ...state, isFetching: false, items: action.episodes };
    default:
        return state;
    }
}

export function podcasts(state={}, action) {
    switch (action.type) {
    case RECEIVE_PODCASTS:
        return { ...state, items: action.podcasts };
    default:
        return state;
    }
}
