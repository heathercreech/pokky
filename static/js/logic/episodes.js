import React from 'react';
import { connect } from 'react-redux';
import { fetchEpisodes } from '../actions/api';

import { Episode } from '../visual/episode';
import { Playing } from './playing';


export class Episodes extends React.Component {
    constructor(props) {
        super(props);
        this.props.fetch();
    }

    render() {
        return (
            <div className='flex-container justify-center'>
                {this.props.episodes.map((episode, index) => (
                    <Episode
                        key={index}
                        src={episode.audio_url}
                        title={episode.title}
                    />
                ))}
                <Playing />
            </div>
        );
    }
}
Episodes.defaultProps = { episodes: [] };


const mapStateToProps = (state) => ({
    episodes: state.episodes.items
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    fetch: () => dispatch(fetchEpisodes(ownProps.match.params.podcast))
});

export const EpisodesPage = connect(mapStateToProps, mapDispatchToProps)(Episodes);
