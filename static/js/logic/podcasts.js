import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { fetchFromApi, receivePodcasts } from '../actions/api';

export class Podcasts extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.props.fetch();
    }

    render() {
        return (
            <div className='flex-container justify-center'>
                {this.props.podcasts.map((podcast, index) => (
                    <div
                        key={index}
                        className='flex-sm-7 padding-normal margin-top-loosest border-thin border-dark-grey text-loose text-bold'
                        onClick={() => this.props.link(podcast.id)}
                    >
                        <div className='flex-container align-center'>
                            <div className='flex-sm-2'>
                                <img src={podcast.image} />
                            </div>
                            <div className='flex-sm-8 text-center'>
                                {podcast.title}:
                                {podcast.feed_url}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        );
    }
}
Podcasts.defaultProps = { podcasts: [] };


const mapStateToProps = (state) => ({
    podcasts: state.podcasts.items
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    fetch: () => dispatch(fetchFromApi(
        `/api/v1/podcast/search${ownProps.location.search}`, receivePodcasts
    )),
    link: (id) => dispatch(push(`/${id}/episodes`))
});

export const PodcastsPage = connect(mapStateToProps, mapDispatchToProps)(Podcasts);
