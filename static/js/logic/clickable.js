import React from 'react';
import PropTypes from 'prop-types';


export class Clickable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div onClick={this.onClick.bind(this)}>{children()}</div>;
    }

    onClick(e) {
        let element = e.target;
        let dimensions = element.getBoundingClientRect();
        let x = e.clientX - dimensions.left;

        if(this.props.onClick) {
            this.props.onClick(x/element.offsetWidth);
        }
    }
}
