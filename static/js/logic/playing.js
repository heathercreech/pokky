import React from 'react';
//import PropTypes from 'prop-types';

import { DockAudioButton, DockNextButton, DockPrevButton } from '../visual/controls';
import { BottomDock } from '../visual/dock';
import { FullHeightGrid as Grid, GridItem } from 'visual/grid';
import { ProgressBar } from 'visual/progress-bar';

export class Playing extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <BottomDock>
                <GridItem small={3} />
                <GridItem small={6}>
                    <Grid justify='center' align='center'>
                        <GridItem small={6} className='text-center'>
                            <DockPrevButton />
                            <DockAudioButton />
                            <DockNextButton />
                        </GridItem>
                        <GridItem small={12}>
                            <ProgressBar height='5px' emptyColor='dark-grey' fillColor='light-grey' percent={0.23}/>
                        </GridItem>
                    </Grid>
                </GridItem>
                <GridItem small={3} />
            </BottomDock>
        );
    }
}



