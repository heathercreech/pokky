import React from 'react';
import {
    TiMediaPlayOutline,
    TiMediaPauseOutline,
    TiChevronLeftOutline,
    TiChevronRightOutline
} from 'react-icons/lib/ti';
// import { Grid } from 'visual/grid';


export const PlayButton = ({ ...rest }) => (
    <TiMediaPlayOutline { ...rest } />
);

export const PauseButton = ({ ...rest }) => (
    <TiMediaPauseOutline { ...rest } />
);

export const NextButton = ({ ...rest }) => <TiChevronRightOutline { ...rest }/>;
export const PrevButton = ({ ...rest }) => <TiChevronLeftOutline { ...rest }/>;

export const PlayOrPause = ({ playing, ...rest }) => {
    if (playing) {
        return <TiMediaPauseOutline { ...rest } />;
    } else {
        return <TiMediaPlayOutline { ...rest } />;
    }
};


export const DockAudioButton = ({ ...rest }) => (
    <PlayOrPause style={{ height: '25px', width: '25px', cursor: 'pointer' }}  { ...rest } />
);

export const DockNextButton = ({ ...rest }) => (
    <NextButton style={{ height: '15px', width: '15px', cursor: 'pointer' }} { ...rest } />
);

export const DockPrevButton = ({ ...rest }) => (
    <PrevButton style={{ height: '15px', width: '15px', cursor: 'pointer' }} { ...rest } />
);
