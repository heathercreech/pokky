import React from 'react';
import { Audio } from 'logic/audio';
import { ProgressBar } from 'logic/progress';
import { TiMediaPlayOutline, TiMediaPauseOutline } from 'react-icons/lib/ti';

const PlayButton = ({ playing, onClick }) => (
    <div onClick={onClick} className={`flex-sm-2 flex-container align-center ${(playing) ? '' : ''}`} style={{cursor: 'pointer', height: '50px', width: '50px'}}>
        {(playing) ? <TiMediaPauseOutline /> : <TiMediaPlayOutline /> }
    </div>
);
// <span className={`flex-sm-12 glyphicon glyphicon-${(playing) ? 'pause' : 'play'}`} {...rest}></span>
export const Episode = ({title, ...rest}) => (
    <Audio {...rest} >
        {({ playing, position }, audio) => (
            <div className='flex-sm-3 text-loose border-thin border-dark-grey text-strong text-larger margin-loose padding-loosest'>
                <div className='flex-container align-center justify-between'>
                    <PlayButton onClick={() => (playing) ? audio.pause() : audio.play()} playing={playing} />
                    <span className='flex-sm-8 text-normal text-bold'>{title}</span>
                    <div className='margin-top-normal flex-sm-12'>
                        <ProgressBar onClick={(percentage) => audio.seek(audio.duration() * percentage)} label=' ' background='midnight' fill='dusk' height='10px' progress={100 * (position / (audio.duration() ? audio.duration() : 1))}/>
                    </div>
                </div>
            </div>
        )}
    </Audio>
);
