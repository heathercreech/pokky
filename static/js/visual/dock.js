import React from 'react';
import PropTypes from 'prop-types';

import { FullHeightGrid as Grid } from 'visual/grid';


export const StickyBlock = ({ className, children, ...rest }) => (
    <div className={ `sticky grey ${className}` } { ...rest }>
        { children }
    </div>
);
StickyBlock.propTypes = { className: PropTypes.string, children: PropTypes.node };


export const BottomDock = ({ children, ...rest}) => (
    <StickyBlock
        className='hug-bottom full-width'
        style={{ height: '80px', opacity: 0.95 }}
        { ...rest }
    >
        <Grid>
            { children }
        </Grid>
    </StickyBlock>
);
BottomDock.propTypes = { height: PropTypes.string, children: PropTypes.node };
