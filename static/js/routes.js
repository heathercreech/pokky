import React from 'react';

import { EpisodesPage } from './logic/episodes';
import { PodcastsPage } from './logic/podcasts';
import { Playground } from './playground';

const HomePage = () => <div>Home page</div>;
const routes = [
    { path: '/:podcast/episodes', app: EpisodesPage },
    { path: '/search', app: PodcastsPage },
    { path: '/playground', app: Playground },
    { path: '/', app: HomePage }
];
export { routes };
