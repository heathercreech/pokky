import { renderPage } from 'logic/one-page';

import { routes } from './routes';
import * as reducers from './reducers';


global.app = {
    run: () => {
        renderPage(routes, reducers, 'light-grey text-charcoal');
    }
};
