
def podcast_endpoints(config):
    config.add_route('api_podcast_episodes', '/episodes')
    config.add_route('api_podcast_episode', '/episodes/{id}')


def consumer_endpoints(config):
    config.add_route('api_consumer_progress', '/progress')
    config.add_route(
        'api_consumer_episode_progress',
        '/progress/{episode_id}'
    )


def api_routes(config):
    config.add_route('api_podcast_search', '/podcast/search')
    config.include(podcast_endpoints, '/podcast/{podcast_id}')
    config.include(consumer_endpoints, '/consumer/{user_id}')


def includeme(config):
    config.add_static_view(
        'static',
        '/var/www/static-content/public',
        cache_max_age=3600
    )

    config.add_route('playground', '/playground')
    config.add_route('home', '/')
    config.add_route('podcast', '/podcast/{id}')
    config.add_route('test', '/test')
    config.add_route('gql', '/gql')
    config.include(api_routes, route_prefix='/api/v1')
    config.add_route('all', '/{path:.*}')
