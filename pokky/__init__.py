from pyramid.config import Configurator
from sqlalchemy import engine_from_config
import logging

from pokky.models.base import init_sql

log = logging.getLogger('root')


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    log.debug(settings)
    config = Configurator(settings=settings)

    config.include('pyramid_jinja2')
    config.add_jinja2_renderer('.html')

    config.include('.routes')

    config.scan()

    engine = engine_from_config(settings)
    init_sql(engine)

    return config.make_wsgi_app()
