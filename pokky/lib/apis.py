from dappy import API, Endpoint

# Declare external APIs here (maybe internals at some point?)


ItunesAPI = API('itunes.apple.com', [
    Endpoint(
        'search', '/search',
        defaults={
            'term': 'Star Wars Minute',
            'entity': 'podcast'
        },
        query_map={ 'search_string': 'term' }
    ),
    Endpoint('get', '/lookup', defaults={'id': '656270845'})
])


class PokkyAPI:
    def __init__(self, request):
        self.params = request.params
        # Since we're only putting int values into the url at the moment
        for key, value in request.matchdict.items():
            setattr(self, key, int(value))
