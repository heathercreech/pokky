import requests

from pokky.lib.rss import RssFeed
from pokky.models.podcasts import Podcast, Episode
from pokky.models.search import Phrase


class PodcastProvider():
    api = None
    name = ''  # Maps to an id from our provider table in the db
    feed_model = RssFeed

    # Searches the provider's podcasts
    @classmethod
    def search(cls, **kwargs):
        data = []
        if 'search_string' in kwargs:
            search_string = kwargs['search_string']
            if Phrase.has(search_string):
                print('RETRIEVING PODCASTS FROM DATABASE')
                data = Podcast.search(search_string)
            else:
                print('INGESTING NEW PODCASTS')
                raw = cls.api.search(kwargs)
                data = cls._format_search(raw)
                Podcast.add(data)
                Phrase.add(search_string)
                data = Podcast.search(search_string)
        return data

    # Retrieve a podcast's episodes from it's RSS feed
    @classmethod
    def get_episodes(cls, podcast):
        meta = {}
        episodes, count = Episode.get(podcast.id)
        if count == 0:
            print('PROCESSING RSS FEED')
            feed_data = cls.load_feed(podcast.feed_url)
            raw = [item.__dict__ for item in feed_data.items]

            # Leaving this format function call in here in case we
            # want to use it at some point
            episodes = cls._format_get_episodes(raw)
            meta = feed_data.metadata.__dict__
            for episode in episodes:
                episode.update({
                    'podcast': podcast.id,
                    'provider': podcast.provider
                })
            meta.update({'id': podcast.id, 'provider': podcast.provider})
            Episode.add(episodes)
            Podcast.add([meta])
            episodes, count = Episode.get(podcast.id)
        else:
            print('SKIPPED PROCESSING RSS FEED')

        episodes = [
            {
                key: value
                for key, value in episode.__dict__.items()
                if not key.startswith('_')
            }
            for episode in episodes
        ]
        return episodes, meta

    @classmethod
    def load_feed(cls, feed_url):
        if feed_url is None:
            feed_url = ''  # TODO: Add a default (cls.get_feed?)
        return cls.feed_model.load(requests.get(feed_url).text)

    # Overridden by children to format search results
    @classmethod
    def _format_search(cls, raw):
        return raw

    # Overridden by children to format a get result
    @classmethod
    def _format_get_episodes(cls, raw):
        return raw
