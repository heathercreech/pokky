from pokky.lib.rss import RssFeed, PodcastRssItem, RssMetadata
from pokky.lib.providers.base import PodcastProvider
from pokky.models.podcasts import Provider
from pokky.lib.apis import ItunesAPI


def get_seconds(t):
    if type(t) is str and ':' in t:
        time_segments = t.split(':')
        if len(time_segments) == 2:
            m, s = time_segments
            h = 0
        else:
            h, m, s = time_segments
        return int(h) * 3600 + int(m) * 60 + int(s)
    else:
        return float(t)


class ItunesRssMetadata(RssMetadata):

    def map(self, item_tag):
        data = super(ItunesRssMetadata, self).map(item_tag)
        data.update(ItunesFeed.findvalue(item_tag, [
            'author',
            'summary',
            'explicit',
            'keywords',
            'subtitle'
        ], namespace='itunes'))

        owner_tag = ItunesFeed.find(item_tag, 'owner', 'itunes')
        if owner_tag is not None:
            data.update(ItunesFeed.findvalue(owner_tag, [
                ('name', 'owner_name'),
                ('email', 'owner_email')
            ], namespace='itunes'))

        if data['explicit'] == 'clean':
            data['explicit'] = False
        return data


class ItunesRssItem(PodcastRssItem):

    def map(self, item_tag):
        data = super(ItunesRssItem, self).map(item_tag)
        data.update(ItunesFeed.findvalue(item_tag, [
            'summary',
            'explicit',
            'duration',
            'keywords',
            'subtitle'
        ], namespace='itunes'))

        if data['duration'] is not None:
            data['duration'] = get_seconds(data['duration'])

        if data['explicit'] == 'clean':
            data['explicit'] = False

        return data


class ItunesFeed(RssFeed):
    namespaces = {
        'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd'
    }
    metadata_model = ItunesRssMetadata
    item_model = ItunesRssItem


class ItunesProvider(PodcastProvider):
    api = ItunesAPI
    name = 'itunes'
    feed_model = ItunesFeed

    @classmethod
    def _format_search(cls, raw):
        return [{
            'provider': Provider.get_by_key(cls.name).id,
            'provider_id': result['collectionId'],
            'title': result['collectionName'],
            'author': result['artistName'],
            'image': result['artworkUrl100'],
            'feed_url': result['feedUrl']
        } for result in raw['results'] if 'feedUrl' in result]
