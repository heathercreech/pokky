from dateutil import parser as date_parser
# I'd use feedparser, but it doesn't use defusedxml
from defusedxml import ElementTree


# Maps a tag from an RSS feed to an object
class RssItem:

    def __init__(self, item_tag):
        self.__dict__ = self._map_from_tag(item_tag)
        for key, value in self.__dict__.items():
            setattr(self, key, value)

    def __json__(self, request):
        return self.__dict__

    # Subclasses override this
    def map(self, item_tag):
        return {}

    def _map_from_tag(self, item_tag):
        if item_tag:
            return self.map(item_tag)


class RssMetadata(RssItem):

    def map(self, item_tag):
        data = super(RssMetadata, self).map(item_tag)
        data.update(RssFeed.findvalue(item_tag, [
            'title',
            'description',
            'copyright',
            'language',
            'link'
            # 'managingEditor'
        ]))

        # We're handling this in ItunesProvider mostly
        '''
        image_tag = item_tag.find('image')
        if image_tag is not None:
            data.update(RssFeed.findvalue(image_tag, [
                {'from': 'url', 'to': 'image'}
            ]))
        '''
        return data


class PodcastRssItem(RssItem):

    def map(self, item_tag):
        data = super(PodcastRssItem, self).map(item_tag)
        data.update(RssFeed.findvalue(item_tag, [
            'title',
            'description',
            {
                'from': 'pubDate', 'to': 'publish_date',
                'func': (
                    lambda v: date_parser.parse(v)
                )
            },
            'guid',
            (
                'enclosure',
                'audio',
                ('url', 'type')
            )
        ]))

        if 'audio_type' in data and len(data['audio_type']) > 16:
            data['audio_type'] = None
        return data


class RssFeed:
    namespaces = {}
    metadata_model = RssMetadata
    item_model = PodcastRssItem

    def __init__(self):
        self.metadata = None
        self.items = None

    # Loads supplied raw rss text into the RssFeed
    @classmethod
    def load(cls, rss_text):
        loaded_feed = cls()

        items = None
        root = ElementTree.fromstring(rss_text)
        if root is not None:
            channel = root.find('channel')
            if channel is not None:
                items = channel.findall('item')
        loaded_feed.metadata = cls._load_metadata(channel)
        loaded_feed.items = cls._load_items(items)
        return loaded_feed

    @classmethod
    def find(cls, item, tag_name, namespace=None):
        return item.find(cls._buildtag(tag_name, namespace))

    # Takes a tag object and a list of tags to retrieve values from
    # tags can be a tuple of the form (tag_name, tag_attr)
    @classmethod
    def findvalue(cls, item, tags, namespace=None):
        data = {}
        if item:
            for tag in tags:
                raw_tag = tag  # What we actually pass through to find
                key = tag  # Pretty tag name (no namespace)
                tag_attr = None
                mod_func = None

                if type(tag) is tuple:
                    raw_tag = tag[0]
                    key = tag[1]
                    if len(tag) > 2:
                        tag_attr = tag[2]
                elif type(tag) is dict:
                    raw_tag = tag['from']
                    key = tag['to']
                    if 'attrs' in tag:
                        tag_attr = tag['attrs']
                    if 'func' in tag:
                        mod_func = tag['func']
                raw_tag = cls._buildtag(raw_tag, namespace)

                # Default our mod_func down here (to avoid flake errors)
                if mod_func is None:
                    def mod_func(v): return v

                if tag_attr:
                    attr = cls.findattr(item, raw_tag, key, tag_attr)
                    if attr is not None:
                        data.update(attr)
                else:
                    data[key] = mod_func(item.findtext(raw_tag))
        else:
            # We might want the individual tags to be set to None instead of
            # just returning None at somepoint -- keeping this around
            # for tag in tags:
            #     data[tag] = None
            data = None
        return data

    # Search container for tag_name and retrieve specific attribute
    @classmethod
    def findattr(self, container, tag_name, pretty_name, tag_attr):
        tag = container.find(tag_name)
        if tag is not None:
            if type(tag_attr) is tuple:
                attr_dict = {}
                for attr in tag_attr:
                    attr_dict[pretty_name + '_' + attr] = tag.get(attr, None)
                return attr_dict
            return tag.get(tag_attr, None)

    @classmethod
    def _load_metadata(cls, channel):
        if channel:
            return cls.metadata_model(channel)

    # Load items into our list with the specified item_model
    @classmethod
    def _load_items(cls, item_tags):
        if item_tags:
            items = []
            for item_tag in item_tags:
                items.append(cls.item_model(item_tag))
            return items

    # Formats a tag's name with a namespace if supplied
    # ElementTree's handling of namespaces is so annoying...
    @classmethod
    def _buildtag(cls, tag_name, namespace=None):
        if namespace in cls.namespaces:
            return '{{{0}}}{1}'.format(cls.namespaces[namespace], tag_name)
        else:
            return tag_name
