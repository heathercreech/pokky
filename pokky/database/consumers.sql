DROP SCHEMA IF EXISTS consumers CASCADE;
CREATE SCHEMA IF NOT EXISTS consumers;


set search_path = consumers, public;

CREATE TABLE consumer (
    id INTEGER,
    name VARCHAR(120),
    password VARCHAR(512), -- SHA3-512

    PRIMARY KEY (id)
);
INSERT INTO consumer VALUES (1, '', '');


-- Name?
CREATE TABLE progress (
    user_id INTEGER REFERENCES consumer(id) ON DELETE CASCADE,
    episode_id INTEGER REFERENCES podcasts.episode(id) ON DELETE CASCADE,
    position REAL,
    finished BOOLEAN,
    PRIMARY KEY (user_id, episode_id)
);
