DROP SCHEMA IF EXISTS podcasts CASCADE;
CREATE SCHEMA IF NOT EXISTS podcasts;


set search_path = podcasts, public;

CREATE TABLE provider (
    id BIGSERIAL,
    key VARCHAR(80),

    PRIMARY KEY (id)
);

INSERT INTO provider (key) VALUES
    ('itunes')
;


CREATE TABLE language (
    code VARCHAR(5),  -- Code used by RSS feeds
    name VARCHAR(50),

    PRIMARY KEY (code)
);

CREATE TABLE podcast (
    id BIGSERIAL,

    provider BIGINT references provider(id),
    provider_id BIGINT,

    feed_url VARCHAR(2083),
    site_url VARCHAR(2083),

    title VARCHAR(150),
    author TEXT,
    subtitle TEXT,
    summary TEXT,
    keywords TEXT,

    image VARCHAR(2083),

    owner_name VARCHAR(150),
    owner_email VARCHAR(254),

    language_code VARCHAR(5) REFERENCES language(code),
    copyright TEXT,
    explicit BOOLEAN,

    PRIMARY KEY (id)
);

CREATE TABLE episode (
    id BIGSERIAL,
    podcast BIGINT,

    guid VARCHAR(2083),

    title TEXT,
    subtitle TEXT,
    description TEXT,
    summary TEXT,
    keywords TEXT,

    publish_date TIMESTAMP WITH TIME ZONE,

    audio_type VARCHAR(16),
    audio_url VARCHAR(2083),
    duration REAL,

    explicit BOOLEAN,

    PRIMARY KEY (id),
    FOREIGN KEY (podcast) REFERENCES podcast (id) ON DELETE CASCADE
);
