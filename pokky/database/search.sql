DROP SCHEMA IF EXISTS search CASCADE;
CREATE SCHEMA IF NOT EXISTS search;


set search_path = search, public;


-- Track which queries have been submitted in search
CREATE TABLE phrase (
    text TEXT,
    PRIMARY KEY (text)
);
