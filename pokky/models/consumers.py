from pokky.models.base import Base, session


class ConsumersBase(Base):
    __abstract__ = True
    __table_args__ = {'schema': 'consumers'}

    @classmethod
    def all(cls):
        return session.query(cls).all()

    @classmethod
    def add(cls, records):
        new_records = []
        for record in records:
            new_record = cls()
            for key, value in record.items():
                setattr(new_record, key, value)
            session.merge(new_record)
            new_records.append(new_record)
        session.commit()
        return new_records


class Progress(ConsumersBase):
    __tablename__ = 'progress'

    @classmethod
    def get(cls, user_id, episode_id):
        q = session.query(cls)
        q = q.filter(cls.user_id == user_id)
        q = q.filter(cls.episode_id == episode_id)
        return q.one_or_none()
