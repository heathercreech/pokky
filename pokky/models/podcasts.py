from pokky.models.base import Base, session


class PodcastsBase(Base):
    __abstract__ = True
    __table_args__ = {'schema': 'podcasts'}

    @classmethod
    def all(cls):
        return session.query(cls).all()

    @classmethod
    def get(cls, id):
        q = session.query(cls)
        q = q.filter(cls.id == id)
        return q.one_or_none()

    @classmethod
    def add(cls, records):
        new_records = []
        for record in records:
            new_record = cls()
            for key, value in record.items():
                setattr(new_record, key, value)
            session.merge(new_record)
            new_records.append(new_record)
        session.commit()
        return new_records


class Podcast(PodcastsBase):
    __tablename__ = 'podcast'

    @classmethod
    def exists(cls, id):
        q = session.query(cls)
        q = q.filter(cls.id == id)
        return q.exists().scalar()

    @classmethod
    def search(cls, search_string):
        q = session.query(cls)
        q = q.filter(cls.title.ilike('%{0}%'.format(search_string)))
        q = q.order_by(cls.title)
        return q.all()

    @classmethod
    def get(cls, id):
        q = session.query(cls)
        q = q.filter(cls.id == id)
        return q.one_or_none()


class Episode(PodcastsBase):
    __tablename__ = 'episode'

    @classmethod
    def get(cls, podcast):
        q = session.query(cls)
        q = q.filter(cls.podcast == podcast)
        return q.all(), q.count()


class Language(PodcastsBase):
    __tablename__ = 'language'


class Provider(PodcastsBase):
    __tablename__ = 'provider'

    @classmethod
    def get_by_key(cls, key):
        q = session.query(cls.id)
        q = q.filter(cls.key == key)
        return q.one()
