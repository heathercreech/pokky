from pokky.models.base import Base, session


class SearchBase(Base):
    __abstract__ = True
    __table_args__ = {'schema': 'search'}

    @classmethod
    def all(cls):
        return session.query(cls).all()

    @classmethod
    def add(cls, records):
        new_records = []
        for record in records:
            new_record = cls()
            for key, value in record.items():
                setattr(new_record, key, value)
            session.merge(new_record)
            new_records.append(new_record)
        session.commit()
        return new_records


class Phrase(SearchBase):
    __tablename__ = 'phrase'

    # Checks if the table has all the keywords
    # supplied in search_string (delimited by spaces)
    @classmethod
    def has(cls, search_string):
        q = session.query(cls)
        q = q.filter(search_string == cls.text)
        return q.count() > 0

    @classmethod
    def add(cls, search_string):
        new_record = cls(text=search_string)
        session.merge(new_record)
        session.commit()
