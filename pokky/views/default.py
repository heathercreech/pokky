import traceback

from pyramid.view import view_config

from pokky.lib.providers import ItunesProvider
from pokky.models.podcasts import Podcast
from pokky.models.consumers import Progress


@view_config(route_name='all', renderer='layouts/_one-page.html')
def t(request):
    return {'name': 'pokky'}


@view_config(route_name='home', renderer='search.html')
def home(request):
    print('Search: ' + request.params.get('q', 'Campaign'))
    results = ItunesProvider.search(search_string=request.params.get(
        'q', 'Campaign'
    ))

    results = [{
        'id': result.id,
        'title': result.title,
        'image': result.image,
        'feed_url': result.feed_url
    } for result in results]
    return {'items': results, 'name': 'pokky'}


@view_config(route_name='podcast', renderer='podcast.html')
def podcast(request):
    podcast_id = request.matchdict['id']
    podcast = Podcast.get(podcast_id)
    episodes = meta = None

    try:
        episodes, meta = ItunesProvider.get_episodes(podcast)
        for episode in episodes:
            progress = Progress.get(1, episode['id'])
            if progress is not None:
                episode['progress'] = progress.position
                episode['finished'] = progress.finished
            else:
                episode['progress'] = 0
            episode['publish_date'] = episode['publish_date'].strftime(
                '%a, %d %b %Y %X %z'
            )
    except Exception as e:
        traceback.print_exc()
        print('Error: ' + podcast.feed_url)
    return {
        'name': 'pokky',
        'podcast': {'episodes': episodes, 'meta': meta}
    }


@view_config(route_name='playground', renderer='layouts/_one-page.html')
def playground(request):
    return {'name': 'pokky'}
