from pyramid.view import view_config, view_defaults

from pokky.models.consumers import Progress


@view_defaults(route_name="api_consumer_episode_progress", renderer='json')
class ProgressAPI():
    def __init__(self, request):
        self.params = request.params
        # Since we're only putting int values into the url at the moment
        for key, value in request.matchdict.items():
            setattr(self, key, int(value))

    @view_config(route_name="api_consumer_progress")
    def get_all_progress(self):
        results = Progress.all()
        resp = []
        for result in results:
            resp.append({
                'user_id': result.user_id,
                'episode_id': result.episode_id,
                'position': result.position,
                'finished': result.finished
            })
        return resp

    @view_config(request_method='GET')
    def get_episode_progress(self):
        episode = Progress.get(self.user_id, self.episode_id)
        if episode is not None:
            print('GOT PODCAST PROGRESS: \t\t{0}'.format(episode.position))
            finished = episode.finished
            if finished is None:
                finished = False
            return {
                'episode': {
                    'position': episode.position,
                    'finished': finished
                }
            }
        else:
            return {'episode': {'position': 0, 'finished': False}}

    @view_config(request_method='POST')
    def add_episode_progress(self):
        try:
            pos = self.params.get('position', 0)
            finished = self.params.get('finished', False)
            Progress.add([{
                'user_id': self.user_id,
                'episode_id': self.episode_id,
                'position': pos,
                'finished': finished
            }])
            print('UPDATED PODCAST PROGRESS: \t{0}'.format(pos))
            return {'status': 200}
        except:
            return {'status': 500}
