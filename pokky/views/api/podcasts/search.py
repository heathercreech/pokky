from pyramid.view import view_config, view_defaults

from pokky.lib.providers import ItunesProvider
from pokky.lib.apis import PokkyAPI


@view_defaults(route_name="api_podcast_search", renderer='json')
class SearchAPI(PokkyAPI):

    @view_config(request_method='GET')
    def search(self):
        results = ItunesProvider.search(search_string=self.params.get(
            'q', 'Campaign'
        ))

        print(self.params.get('q', 'Campaign'))

        results = [{
            'id': result.id,
            'title': result.title,
            'image': result.image,
            'feed_url': result.feed_url
        } for result in results]
        return {'podcasts': results}
