from pyramid.view import view_config, view_defaults

from pokky.models.podcasts import Podcast
from pokky.lib.providers import ItunesProvider
from pokky.lib.apis import PokkyAPI


@view_defaults(route_name="api_podcast_episodes", renderer='json')
class EpisodesAPI(PokkyAPI):

    @view_config(request_method='GET')
    def get_episodes(self):
        podcast = Podcast.get(self.podcast_id)
        episodes, meta = ItunesProvider.get_episodes(podcast)

        for episode in episodes:
            episode['publish_date'] = episode['publish_date'].strftime(
                '%a, %d %b %Y %X %z'
            )

        return {
            'episodes': episodes,
            'meta': meta
        }
        return {'status': 500}
